### Typescript

#### instalar o compilador do typescript e configurar

##### iniciando um pachage.json
`npm init`

##### install typescript with dev dependency
`npm install typescript --save-dev`

the typescript must be listed in package.json

##### nomear os arquivos de manipulação para ts ao inves de js
todos os arquivos serão escritos em ts e compilados para js

##### criar o arquivo de configuração de typescript
`tsconfig.json`
o compilador antes de ser executado sempre olhará para esse arquivo

configuração mínima para o tsconfig.json

    {
        "compilerOptions": {
            "target": "es6",
            "outDir": "app/js"
        },
        "include": [
            "app/ts/**/*"
        ]
    }

always after create a tsconfig.json, close and reopen vscode

##### incluir o script de execução do compiler no package.json 

    "scripts": {
        "compile": "tsc"
    },
##### executando o compilador ts
`npm run compile`

in js we doesn't need to declare class's attributes, just in the constructor
but in ts we need it

when we say a property is private in ts, we cannot change the access modifier straight
but when we compile a ts it allow js be execute if an error be in ts
to avoid that compilation in case of ts's error we put a line in tsconfig.json
that just compile if the ts doesn't have errors
`"noEmitOnError": true` inside compilerOptions

##### To automatically compile ts into js using a watch change
in `package.json` scripts, insert the script
`"start": "tsc -w"`
and the flag -w means to watch for every change in ts directory

to run the command start we don't need the "run"
`npm start`

AULA 02

create a NegociacaoController
importar o controller na index.html
no app.js instanciar o controller

AULA 03 - TIPAGEM

if we don't put a type in a property its type always going to be "any"
to tell ts compile to not allow "any" type we have to put in "compileOptions" tsconfig.json the code
`"noImplicitAny": true`
it make our code be with a lot of error because we haven't typing any class property till now

the benefits of typing properties is that we have access to all methods that the class type offers

string and number int tyni means it is a literal and keep a primitive value
Se we use them in Uppercase the type is object.

Element

All elements catch from the dom is the type of Element
`private _inputData: Element;`
`this._inputData = document.querySelector('#data');`

but Element is generic and we need to be more especific 
Because the Element doesn't have the property value that interests to us
get data from input

being that we can use a HTMLInputElement that has a property value
`private _inputData: HTMLInputElement;`

but we have to casting the element through <Type>
`this._inputData = document.querySelector('#data');`
to
`this._inputData = <HTMLInputElement>document.querySelector('#data');`

Event
event from js is the type Event
`adiciona(event: Event){}`

date, Number and Float
To ensure that the string get from the element is a number or a float
we must parse its value
`parseInt(number)`
`parseFloat(number)` 
`new Date(this._inputData.value.replace(/-/g,',')),`

AULA 03

criada negociacoes para adicionar uma nova negociação no tabela
somente com metodo de adicionar e getArray para exibição
importar as negociações abaixo de negociação no index.html

INSTEAD use type `property: Array<Negociacao>`
we has a shortcut as `property: Negociacao[]`

if we pass a value in a born of a Class property it already has its type and we needn't explicit it
`private _negociacoes: Negociacoes = new Negociacoes();` to
`private _negociacoes              = new Negociacoes();`

adicionar as negociacoes no controller as a property
ao criar uma instancia negociação no controller adicionar ela no array de negociações
`this._negociacoes.adiciona(negociacao);`

apos adicionar uma negociacao no array imprimir ela atraves do foreach

    this._negociacoes.paraArray().forEach(negociacao => {
        console.log(negociacao.data)
        console.log(negociacao.quantidade)
        console.log(negociacao.valor)
        console.log(negociacao.volume)
    }) 

para garantir que ninguem possa alterar nosso array 
passamos um novo arrau sempre que for adicionado uma negociação
e no mesmo metodo indicamos qual será o tipo do retorno

    paraArray(): Negociacao[] {
        return [].concat(this._negociacoes);
    }

colocar tipos em todos os metodos

DOM
imperativa = tem que manipular cada mutação do dom
declarativa = pega um atraves de um template e gera a manipulação do dom sem entrar em detalhes de cada parte que se quer mudar
O react usa a forma declarativa

Manipulação do dom de forma declarativa
criar um novo arquivo NegociacoesView.ts
importar ele no index.html
devolver uma tabela como um template string
`return '`string`'`
incluir `private _negociacoesView = new NegociacoesView(seletor)` como propriedade de NegociacoesController, passando o seletor para a instanciação

para renderizar um template através de "declarativa"
usamos a propriedade do Element.InnerHTML
que passando um template ele transforma isso em elementos do Dom

A classe NegociacoesView

    class NegociacoesView {
        private _elemento: HTMLDivElement;

        constructor(selector: string){
            this._elemento = document.querySelector(selector);
        }

        update(negociacoes: Negociacoes): void {
            this._elemento.innerHTML = this.template(negociacoes);
        }
        
        template(negociacoes: Negociacoes): string {
            return `string`;
        }
    }

assim que a pagina for carregada já queremos renderizar a tabela
portanto no constructor do NegociacaoController informamos isso através do
`this._negociacoesView.update()`

Queremos tb atualizar a renderização todo vez que incluirmos uma nova negociação
então no metodo `NegociacaoController.adiciona()`
passamos `this._negociacoesView.update()`

para renderizarmos os dados das negociacoes passamos como argumento do metodo
`update(this._negociacoes)` as negociacoes
e na View atualizamos o metodo update para receber as negociacoes e para o metodo template que vai manipular os dados de negociacoes incluindo no template

    update(negociacoes: Negociacoes): void {
        this._elemento.innerHTML = this.template(negociacoes);
    }

    template(negociacoes: Negociacoes): string {
        return `

e para renderizar com os dados no template usamos o codigo abaixo 
    <tbody>
        ${negociacoes.paraArray().map(negociacao => {
            return `
                <tr>
                    <td>
                        ${negociacao.data.getDate()}/
                        ${negociacao.data.getMonth()+1}/
                        ${negociacao.data.getFullYear()}
                    </td>
                    <td>${negociacao.quantidade}</td>
                    <td>${negociacao.valor}</td>
                    <td>${negociacao.volume}</td>
                </tr>
            `
        }).join('')}
    </tbody>

usamos o metodo `.join('')` com string vazia pois a cada nova inserção de uma negociação no template ele separa por vírgula.

`${}` isso é uma expressão literal

`${negociacoes.paraArray().map(negociacao => {})` forma de iterar

Podemos ter uma expressão literal dentro de outra,
ele devolve um template que é deficnido dentro de uma expressão literal e dentro dela atraves de uma expressão fazemos um iteração dentro de uma lista para que a cada iteração devolva um adendo de uma expressão literal

Aula 04 Herança

create a MensagemView to inform the user that a negociacao was created
it's quite similar to NegociacoesView
import MensagemView in index.html
<div id="mensagemView"></div> already exist
In NegociacoesController when a negociação is created we call MensagemView to show a message that negociação was created.

`this.mensagemView.update('Negociação adicionada com sucesso!');`
whithout forget to add a new NegociacoesController property
`private mensagemView: MensagemView = new MensagemView('#mensagemView');`

but MensagemView and NegociacoesView is quite similar
lets create a enhance `View`

Generic Type <T>

Mother class
`class View<T>{}`
    `update(mensagem: T): void {`
    `template(mensagem: T): string {`

Child Class
`class MensagemView<string>{}`
    `template(mensagem: string): string {`
    
`class NegociacoesView<Negociacoes>{}`
    `template(negociacoes: Negociacoes): string {`

A class can have more than one generic type
`class Qualquer<T, K>`

to avoid the class View be instantiated we transform it in an abstract class
and its template method as well
`abstract class View<T> {`
`abstract template(mensagem: T): string;`

an abstract method doesn't have implementation therefore/thus/so we use ; as above

AULA 05

install jquery types into typescript
`npm install @types/jquery --save-dev`
this install show a new directory inside node_modules called @types

to remove comments from de compiled code we pass a new config item in tsconfig.json
`removeComments: true`


























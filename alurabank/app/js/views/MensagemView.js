class MensagemView extends View {
    template(mensagem) {
        return `<p class="alert alert-warning">${mensagem}</p>`;
    }
}

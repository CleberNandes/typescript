class View {
    constructor(selector) {
        this._elemento = document.querySelector(selector);
    }
    update(mensagem) {
        this._elemento.innerHTML = this.template(mensagem);
    }
}

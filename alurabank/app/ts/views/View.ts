abstract class View<T> {

    private _elemento: HTMLDivElement;

    constructor(selector: string){
        this._elemento = document.querySelector(selector);
    }

    update(mensagem: T): void {
        this._elemento.innerHTML = this.template(mensagem);
    }

    abstract template(mensagem: T): string;
}